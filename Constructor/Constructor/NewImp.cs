﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    public class NewImp
    {
        public  void  Add()
        {
            Console.WriteLine("form Parent");
        }
        public void Sub()
        {
            Console.WriteLine("form Parent");
        }
        /// <summary>
        /// if virtual method is not override in first child class then it can be available for override in second child class
        /// 
        /// In case of method hiding, P obj=new C(); obj.meth(),Parent version will get called
        /// </summary>
        public virtual void Mult()
        {
            Console.WriteLine("form Parent");
        }

    }

    public class NewImpChild:NewImp
    {
        public  void Add()
        {
            base.Add();
            Console.WriteLine("form Child");
        }

        public void Sub()
        {
            Console.WriteLine("form Child");
        }

    }

    public class NewImpSecondChild:NewImpChild
    {
        public override void Mult()
        {
            Console.WriteLine("form second Child");
        }
    }
}
