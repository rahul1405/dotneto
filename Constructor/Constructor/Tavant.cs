﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    public interface I
    {
        void Do();
    }
    public class Parent
    {
        public void Do()
        {
            Console.WriteLine("hi");
        }
    }

    public class Child:Parent,I
        {
           
        }
}
