﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    public delegate void Print(int value);

    class DelegateCheck
    {
        public static void PrintHelperMethod(Print printDel, int val)
        {
            val += 10;
            printDel(val);
        }

        static void Main1(string[] args)
        {
            PrintHelperMethod(delegate (int val) { Console.WriteLine("Anonymous method: {0}", val); }, 100);
        }
        /// <summary>
        /// /////////////////////////////
        /// </summary>
        /// <param name="value"></param>
        public delegate void Print(int value);

        static void Main2(string[] args)
        {
            int i = 10;

            Print prnt = delegate (int val) {
                val += i;
                Console.WriteLine("Anonymous method: {0}", val);
            };

            prnt(100);
        }

    }



}
