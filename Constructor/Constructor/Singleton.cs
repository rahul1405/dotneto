﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    public interface I1
    {
       void doa();
    }
    public class Singleton
    {
        private static readonly Lazy<Singleton> si = new Lazy<Singleton>(() => new Singleton());
        private static Singleton objSing = null;
        private Singleton()
        {

        }
        private Singleton(int i)
        {

        }
        public int Subtract()
        {
            return 25;
        }
        public static Singleton GetInstance()
        {
            //Singleton objSing1 = new Singleton();//Valid One Can create non-static in Static but cannot access non-static 
            //objSing so we have make it static
            if (objSing == null)
            {
                objSing = new Singleton();
                
            }
            return objSing;
        }
    }

    //Cannot inherit form Class if it has defualt cons as private even if it has parameterized cons as public
    //public class SingChild : Singleton
    //{

    //}
    public static class Sc
    { }
}
