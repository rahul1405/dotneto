﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
   public  class ConsChain
    {
        public ConsChain():this(10)///this will get called first
        {
            Console.WriteLine("non parameter");
        }

        public ConsChain(int i)
        {
            Console.WriteLine("parameter"+i.ToString());
        }

    }

    public class ConsChild:ConsChain
    {

        public ConsChild():base()
        {
            Console.WriteLine("Child Cons");
        }
    }
}
