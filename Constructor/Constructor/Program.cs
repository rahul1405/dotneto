﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsChild cChild = new ConsChild();
            ConsChain cc = new ConsChain();
           
           // Singleton s = Singleton.GetInstance();
            //Singleton s1 = new Singleton(3);
            Employeeservice e = new Employeeservice();
            // var temp= e.EmployeeData(123);
            //  Console.WriteLine(temp);
            //var temp1=  e.Address();
            //  Console.WriteLine(temp1);
            search s1 = new search(e.EmployeeData);
            IAsyncResult ir = s1.BeginInvoke(101, null, null);
            Console.WriteLine(e.Address());
            s1.EndInvoke(ir);







            var anony_object = new
            {
                s_id = 134,
                s_name = "Siya",
                language = "Ruby"
            };

           // anony_object.s_id = 99;

            CC obj = new CC();
            obj.Do(777);
            obj.Do("bbkkjhkjjh");

            Child d = new Child();
            d.Do();
            // B obj = new B();
            // B obj1 = new B();
            MessageService ms = new MessageService();
            EmailService es = new EmailService();
            EventImp eimp = new EventImp();
            eimp.VideoEvent += ms.message;
            eimp.VideoEvent += es.emailmessage;
            eimp.VideoEncode();
            NewImp ni = new NewImpChild();
            ni.Add();
            ni.Mult();
            NewImpChild nic = new NewImpChild();
            nic.Mult();
            nic.Sub();


            NewImp nisc = new NewImpSecondChild();
            nisc.Mult();

            
            // AbsChild objAbsChild = new AbsChild();

            ClassAbs objAbs = new AbsChild();
            objAbs.subtract();

            Singleton objS = Singleton.GetInstance();
            objS.Subtract();
          //  Singleton o = new Singleton(10);///Can create object of class if default cons is private
            YieldCheck objYield = new YieldCheck();
            foreach(int i in objYield.GetNumber())
            {
                Console.WriteLine(i);
            }

            foreach (int i in objYield.GetTotal())
            {
               // int[] b = new int[6];
                //int[] a = new int[6];
               //b= a.Clone();
                //a[1] = 9;
                //a.CopyTo(b,0);
                Console.WriteLine(i);
            }
        }
        
    }

    public struct Coords
    {
        
        public Coords(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; }
        public double Y { get; }

        public override string ToString() => $"({X}, {Y})";
    }
    

    public class A
    {
        static A()
        {
            Console.WriteLine("A static const");
        }

        public  A(int x)
        {
            Console.WriteLine("A const parameterized and called by child");
        }
       
    }

    public class B : A
    {
        static B()
        {
            Console.WriteLine("B static const");
        }

        public B() : base(2)
        {
            Console.WriteLine("B const");
        }
    }
    //If class has private constructor then it can not be inherited due to protection level
    //If class has abstract method then class has to be marked as abstract,please use override
    //1)static child 2)static Parent 3) parent const4)child const
    //When using base against construtor then only base one contructor of parent is called
    //For further overriding, use the sealed keyword
    //Abstract method should not have definition
    public abstract class ClassAbs
    {

        public ClassAbs()
        {
            Console.WriteLine("ClassAbs const");

        }
        public abstract void subtract();


    }

        public class AbsChild:ClassAbs
        {

          public AbsChild()
          {
            Console.WriteLine("AbsChild const");
        }
          public override void subtract()
          {
            Console.WriteLine("Subtract Child");
        }

        }
}
