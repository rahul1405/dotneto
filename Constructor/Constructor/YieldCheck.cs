﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    public class YieldCheck
    {



        public IEnumerable<int> GetNumber()
        {
            List<int> objList = new List<int>();
            objList.Add(1);
            objList.Add(2);
            objList.Add(3);
            objList.Add(4);

            foreach (int i in objList)
            {
                yield return i;
            }

        }

        public IEnumerable<int> GetTotal()
        {
            List<int> objList = new List<int>();
            objList.Add(1);
            objList.Add(2);
            objList.Add(3);
            objList.Add(4);
            var total = 0;
            foreach (int i in objList)
            {
                total += i;
                yield return total;
            }

        }
    }
}
