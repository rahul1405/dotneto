﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    public class PC
    {
        public void Do(int i)
        {
            Console.WriteLine(i);
        }
    }

    public class CC:PC
    {
        public void Do(string s)
        {
            Console.WriteLine(s);
        }
    }
}
