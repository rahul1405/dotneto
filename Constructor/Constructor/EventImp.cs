﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    public class EventImp
    {
        public  delegate void VideoDelg(object obj, EventArgs e);
        public event VideoDelg VideoEvent;

        public void VideoEncode()
        {

            Console.WriteLine("Video Completes");
            VideoNotification();
        
        }

        public void VideoNotification()
        {

            if(VideoEvent!=null)
            {
                VideoEvent(this,EventArgs.Empty);
            }
        }
    }

    public class EventCall
    {
        public void eventRaise()
        {
            MessageService ms = new MessageService();
            EmailService es = new EmailService();
            EventImp eimp = new EventImp();
            eimp.VideoEvent += ms.message;
            eimp.VideoEvent += es.emailmessage;

        }

    }

    public class MessageService
    {
        public void message(object obj,EventArgs ea)
        {
            Console.WriteLine("Hi Message");
        }

    }

    public class EmailService
    {
        public void emailmessage(object obj, EventArgs ea)
        {
            Console.WriteLine("Hi Email");
        }

    }


}
